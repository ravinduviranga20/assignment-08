#include <stdio.h>
struct student {
    char firstName[50];
    int subject;
    float marks;
} s[10];

int main() {
    int i;
    printf("Enter information of students:\n");

    for (i = 0; i < 5; ++i) {
        s[i].subject = i + 1;
        printf("\nFor subject number%d,\n", s[i].subject);
        printf("Enter first name: ");
        scanf("%s", s[i].firstName);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("Displaying Information:\n\n");

    for (i = 0; i < 5; ++i) {
        printf("\nSubject number: %d\n", i + 1);
        printf("First name: ");
        puts(s[i].firstName);
        printf("Marks: %.1f", s[i].marks);
        printf("\n");
    }
    return 0;
}
